import os
import sys
import argparse
import json
import bpy
import mathutils
import math
import random
import bpy_extras

from pathlib import Path


def load_blendfile(name):
    """Load a blendfile into the current scene."""
    # delete all objects
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.delete()

    # Load the blender file into the current scene
    with bpy.data.libraries.load(name) as (data_from, data_to):
        data_to.objects = data_from.objects
        data_to.worlds = data_from.worlds

    # Link the object to the current scene
    for obj in data_to.objects:
        print(obj.name)
        bpy.context.scene.collection.objects.link(obj)

    bpy.context.scene.world = data_to.worlds[0]


def look_at(obj_camera, point):
    """Track the monkey when randomizing the camera."""
    eye = mathutils.Vector([random.uniform(-10, 10) for _ in range(3)])
    obj_camera.location = eye
    bpy.context.view_layer.update()

    location = obj_camera.matrix_world.to_translation()

    direction = point - location
    # point the cameras '-Z' and use its 'Y' as up
    rot_quat = direction.to_track_quat("-Z", "Y")

    # assume we're using euler rotation
    obj_camera.rotation_euler = rot_quat.to_euler()
    bpy.context.view_layer.update()


def extract(reset=False):
    """Extract the camera and light information from the blender scene."""
    camera = bpy.data.objects["Camera"]
    if not reset:
        look_at(camera, bpy.data.objects["Suzanne"].location)
        bpy.context.view_layer.update()

    rotation_matrix = mathutils.Matrix.Rotation(-math.pi / 2.0, 4, "X")
    # set the render resolution to 512x512
    bpy.context.scene.render.resolution_x = 512
    bpy.context.scene.render.resolution_y = 512
    camera_matrix = (rotation_matrix @ camera.matrix_world).inverted()

    dg = bpy.context.evaluated_depsgraph_get()
    render = bpy.context.scene.render

    # Calculate the projection matrix using Blender's calc_matrix_camera
    projection_mat_blender = camera.calc_matrix_camera(
        dg,
        x=render.resolution_x,
        y=render.resolution_y,
        scale_x=render.pixel_aspect_x,
        scale_y=render.pixel_aspect_y,
    )

    # Extract near and far clipping values
    clip_start = camera.data.clip_start
    clip_end = camera.data.clip_end

    # Compute Z scale and offset from the camera's clip values
    z_scale = clip_end / (clip_end - clip_start)
    z_offset = -(clip_start * z_scale)

    # Convert the Blender projection matrix to OpenGL format
    projection_mat_opengl = mathutils.Matrix.Identity(4)
    projection_mat_opengl[1][1] = -1  # Flip Y
    projection_mat_opengl[2][2] = z_scale
    projection_mat_opengl[2][3] = z_offset

    # Multiply matrices to get the final OpenGL projection matrix
    projection_mat_final = projection_mat_blender @ projection_mat_opengl

    camera_data = [[row.x, row.y, row.z, row.w] for row in camera_matrix]
    projection_data = [[row.x, row.y, row.z, row.w] for row in projection_mat_final]

    light = bpy.data.objects["Light"]
    light_matrix = rotation_matrix @ light.matrix_world
    light_position = light_matrix.to_translation()

    export_data = {
        "camera": camera_data,
        "projection": projection_data,
        "light": [v for v in light_position],
    }

    return export_data


def render_image(output_path):
    """Randomly rotate the monkey and render the scene."""
    bpy.context.scene.render.filepath = str(output_path.with_suffix(".png"))
    bpy.context.scene.camera = bpy.data.objects["Camera"]
    bpy.ops.render.render(write_still=True)


if __name__ == "__main__":
    argv = sys.argv[sys.argv.index("--") + 1 :] if "--" in sys.argv else []
    parser = argparse.ArgumentParser(
        description="Extract a camera, light and model information from the blendefile."
    )

    parser.add_argument(
        "--blend-file", type=str, help="Path to blend file.", required=True
    )
    parser.add_argument(
        "--output-file", type=str, help="Path to store data.", required=True
    )

    parser.add_argument(
        "--reset", action="store_true", help="Reset the rotation to (0, 0, 0)."
    )

    args = parser.parse_args(argv)

    output_path = Path(args.output_file)
    if not output_path.parent.exists():
        os.makedirs(output_path.parent)

    load_blendfile(args.blend_file)

    result_data = extract(reset=args.reset)

    with open(output_path, "w") as f:
        json.dump(result_data, f)

    # Render the scene.
    render_image(output_path)
