import numpy as np
import nvdiffrast.torch as dr
import json
import torch
import torchvision
import torchvision.transforms as T
import argparse

from copy import deepcopy
from pathlib import Path
from renderer import obj, material, util, mesh, texture, render
from PIL import Image

np.set_printoptions(suppress=True, precision=3)


# Prepare training data
def load_image(path, size=512):
    """Load image from give path."""
    image = Image.open(path)
    image = T.Compose([T.Resize((size, size)), T.ToTensor()])(image)
    return image[0:3, :, :]


input_path = Path("./output/data.png").resolve()
ref_data = load_image(input_path).unsqueeze(0).cuda()


# Setup rendering context
glCtx = dr.RasterizeGLContext()


def render_optim(
    input_mesh, materials, mvp, campos, lightpos, power=2, render_res=512, scale=2.5
):
    params = {
        "mvp": mvp,
        "lightpos": lightpos,
        "campos": campos,
        "resolution": [render_res, render_res],
        "time": 0,
    }

    ref_mesh = mesh.compute_tangents(input_mesh)
    ref_mesh_aabb = mesh.aabb(ref_mesh.eval())
    unit_mesh = mesh.unit_size(input_mesh)

    background = torch.ones((1, render_res, render_res, 3), dtype=torch.float32).cuda()
    background *= 0.6

    train_mesh = mesh.Mesh(
        input_mesh.v_pos, unit_mesh.t_pos_idx, material=materials, base=unit_mesh
    )
    train_mesh = mesh.auto_normals(train_mesh)
    train_mesh = mesh.compute_tangents(train_mesh)
    train_mesh = mesh.center_by_reference(train_mesh.eval(params), ref_mesh_aabb, scale)

    return render.render_mesh(
        glCtx,
        train_mesh,
        mvp,
        campos,
        lightpos,
        power,
        render_res,
        num_layers=1,
        background=background,
    )


if __name__ == "__main__":
    print("Start training...")

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--input-data", type=str, help="Path to camera parameters", required=True
    )
    parser.add_argument(
        "--ref-image", type=str, help="Path to rendered image", required=True
    )

    args = parser.parse_args()

    input_datapath = Path(args.input_data).resolve()
    ref_datapath = Path(args.ref_image).resolve()

    output_path = input_datapath.parent

    input_mesh = obj.load_obj("data/monkey.obj")
    kd_map = texture.create_trainable(
        deepcopy(input_mesh.material["kd"]), (512, 512), True
    )
    ks_map = texture.create_trainable(
        deepcopy(input_mesh.material["ks"]), (512, 512), True
    )

    rotation = torch.eye(3).cuda().requires_grad_(True)

    train_materials = material.Material(
        {
            "bsdf": input_mesh.material["bsdf"],
            "kd": kd_map,
            "ks": ks_map,
        }
    )
    # if train_texture:
    train_materials.requires_grad_(True)
    params = train_materials.parameters()

    with open(input_datapath, "r") as f:
        data = json.load(f)

    camera = np.array(data["camera"])
    proj_mtx = np.array(data["projection"])

    mvp_matrix = np.matmul(proj_mtx, camera)
    mvp_matrix = mvp_matrix[None, ...]

    lightpos = np.array(data["light"])
    lightpos = lightpos[None, ...]

    campos = np.linalg.inv(camera)[:3, 3]
    campos = campos[None, ...]

    parameters = list(train_materials.parameters())

    # Initialize optimizer and scheduler
    optimizer = torch.optim.Adam(parameters, lr=0.05)
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=200, gamma=0.95)

    ref_data = load_image(ref_datapath).unsqueeze(0).cuda()
    ref_data = ref_data.permute(0, 2, 3, 1)

    # Run training loop
    for i in range(500):
        optimizer.zero_grad()
        result = render_optim(input_mesh, train_materials, mvp_matrix, campos, lightpos)
        if i == 0:
            out_result = result[0].permute(2, 0, 1)
            before_optimization = output_path.joinpath("before_optimization.png")
            torchvision.utils.save_image(out_result, before_optimization)

        loss = torch.nn.functional.mse_loss(result, ref_data)
        loss.backward()
        optimizer.step()
        scheduler.step()
        if i % 10 == 0:
            print("step: {}, loss: {}".format(i, loss.item()))

    # Save the images after optimization
    result = render_optim(input_mesh, train_materials, mvp_matrix, campos, lightpos)
    after_optimization = output_path.joinpath("after_optimization.png")
    result = result[0].permute(2, 0, 1)
    torchvision.utils.save_image(result, after_optimization)

    texture = train_materials["kd"].getMips()[0][0]
    texture = texture.permute(2, 0, 1)

    texture = torch.clip(texture, 0, 1)
    texture_path = output_path.joinpath("texture.png")
    torchvision.utils.save_image(texture, texture_path)
    print("Done!")
