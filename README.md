# Test project
The idea of this project is to extract the scene parameters from the path. 
This project is only tested on Linux systems.


1. Create a virtual environment with a package of your choice.
2. Once the environment is ready, install the requirements
```
pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118

pip install -r requirements.txt
```

3. Then to extract the camera from a blendfile use the following command.
```
blender --background --python extract.py -- --blend-file "monkey.blend" --output-file "./output/data.json"
```
This will also render a file in the folder where `data.json` is specified.

Optionally you can pass `--reset` to the camera to default orientation. By default, the camera is randomized.

4. Once all the extraction is done, run the following script to optimize for the scene
```
python blender.py --input-data "output/data.json" --ref-image "output/data.png"
```

# Results
1. The results will be in the same folder where `data.json` and `data.png` is located
2. `before_optimization.png` shows the texture of the mesh before optimization 
3. `after_optimization.png` shows the texture of the mesh after optimization 
4. There is also `texture.png` that shows how the texture was optimized. Since the camera wasn't rotated during optimization, only the portion being displayed was optimized.

